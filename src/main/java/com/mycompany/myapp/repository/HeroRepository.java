package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Hero;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Hero entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HeroRepository extends JpaRepository<Hero, Long> {
    @Query("select hero from Hero hero where hero.user.login = ?#{principal.username}")
    List<Hero> findByUserIsCurrentUser();
}
