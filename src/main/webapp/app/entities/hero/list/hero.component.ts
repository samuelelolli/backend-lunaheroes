import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHero } from '../hero.model';
import { HeroService } from '../service/hero.service';
import { HeroDeleteDialogComponent } from '../delete/hero-delete-dialog.component';

@Component({
  selector: 'jhi-hero',
  templateUrl: './hero.component.html',
})
export class HeroComponent implements OnInit {
  heroes?: IHero[];
  isLoading = false;

  constructor(protected heroService: HeroService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.heroService.query().subscribe(
      (res: HttpResponse<IHero[]>) => {
        this.isLoading = false;
        this.heroes = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IHero): number {
    return item.id!;
  }

  delete(hero: IHero): void {
    const modalRef = this.modalService.open(HeroDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.hero = hero;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
