package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Hero;
import com.mycompany.myapp.domain.enumeration.Sex;
import com.mycompany.myapp.repository.HeroRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HeroResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class HeroResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final Long DEFAULT_ETA = 1L;
    private static final Long UPDATED_ETA = 2L;

    private static final Sex DEFAULT_SEX = Sex.M;
    private static final Sex UPDATED_SEX = Sex.F;

    private static final String DEFAULT_SEGNI = "AAAAAAAAAA";
    private static final String UPDATED_SEGNI = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/heroes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HeroRepository heroRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHeroMockMvc;

    private Hero hero;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hero createEntity(EntityManager em) {
        Hero hero = new Hero().nome(DEFAULT_NOME).eta(DEFAULT_ETA).sex(DEFAULT_SEX).segni(DEFAULT_SEGNI).note(DEFAULT_NOTE);
        return hero;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hero createUpdatedEntity(EntityManager em) {
        Hero hero = new Hero().nome(UPDATED_NOME).eta(UPDATED_ETA).sex(UPDATED_SEX).segni(UPDATED_SEGNI).note(UPDATED_NOTE);
        return hero;
    }

    @BeforeEach
    public void initTest() {
        hero = createEntity(em);
    }

    @Test
    @Transactional
    void createHero() throws Exception {
        int databaseSizeBeforeCreate = heroRepository.findAll().size();
        // Create the Hero
        restHeroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isCreated());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeCreate + 1);
        Hero testHero = heroList.get(heroList.size() - 1);
        assertThat(testHero.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testHero.getEta()).isEqualTo(DEFAULT_ETA);
        assertThat(testHero.getSex()).isEqualTo(DEFAULT_SEX);
        assertThat(testHero.getSegni()).isEqualTo(DEFAULT_SEGNI);
        assertThat(testHero.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    void createHeroWithExistingId() throws Exception {
        // Create the Hero with an existing ID
        hero.setId(1L);

        int databaseSizeBeforeCreate = heroRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHeroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = heroRepository.findAll().size();
        // set the field null
        hero.setNome(null);

        // Create the Hero, which fails.

        restHeroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEtaIsRequired() throws Exception {
        int databaseSizeBeforeTest = heroRepository.findAll().size();
        // set the field null
        hero.setEta(null);

        // Create the Hero, which fails.

        restHeroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSexIsRequired() throws Exception {
        int databaseSizeBeforeTest = heroRepository.findAll().size();
        // set the field null
        hero.setSex(null);

        // Create the Hero, which fails.

        restHeroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isBadRequest());

        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllHeroes() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        // Get all the heroList
        restHeroMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hero.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].eta").value(hasItem(DEFAULT_ETA.intValue())))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.toString())))
            .andExpect(jsonPath("$.[*].segni").value(hasItem(DEFAULT_SEGNI)))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)));
    }

    @Test
    @Transactional
    void getHero() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        // Get the hero
        restHeroMockMvc
            .perform(get(ENTITY_API_URL_ID, hero.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hero.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.eta").value(DEFAULT_ETA.intValue()))
            .andExpect(jsonPath("$.sex").value(DEFAULT_SEX.toString()))
            .andExpect(jsonPath("$.segni").value(DEFAULT_SEGNI))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE));
    }

    @Test
    @Transactional
    void getNonExistingHero() throws Exception {
        // Get the hero
        restHeroMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHero() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        int databaseSizeBeforeUpdate = heroRepository.findAll().size();

        // Update the hero
        Hero updatedHero = heroRepository.findById(hero.getId()).get();
        // Disconnect from session so that the updates on updatedHero are not directly saved in db
        em.detach(updatedHero);
        updatedHero.nome(UPDATED_NOME).eta(UPDATED_ETA).sex(UPDATED_SEX).segni(UPDATED_SEGNI).note(UPDATED_NOTE);

        restHeroMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedHero.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedHero))
            )
            .andExpect(status().isOk());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
        Hero testHero = heroList.get(heroList.size() - 1);
        assertThat(testHero.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testHero.getEta()).isEqualTo(UPDATED_ETA);
        assertThat(testHero.getSex()).isEqualTo(UPDATED_SEX);
        assertThat(testHero.getSegni()).isEqualTo(UPDATED_SEGNI);
        assertThat(testHero.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    void putNonExistingHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();
        hero.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHeroMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hero.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hero))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();
        hero.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHeroMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hero))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();
        hero.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHeroMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHeroWithPatch() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        int databaseSizeBeforeUpdate = heroRepository.findAll().size();

        // Update the hero using partial update
        Hero partialUpdatedHero = new Hero();
        partialUpdatedHero.setId(hero.getId());

        partialUpdatedHero.note(UPDATED_NOTE);

        restHeroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHero.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHero))
            )
            .andExpect(status().isOk());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
        Hero testHero = heroList.get(heroList.size() - 1);
        assertThat(testHero.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testHero.getEta()).isEqualTo(DEFAULT_ETA);
        assertThat(testHero.getSex()).isEqualTo(DEFAULT_SEX);
        assertThat(testHero.getSegni()).isEqualTo(DEFAULT_SEGNI);
        assertThat(testHero.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    void fullUpdateHeroWithPatch() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        int databaseSizeBeforeUpdate = heroRepository.findAll().size();

        // Update the hero using partial update
        Hero partialUpdatedHero = new Hero();
        partialUpdatedHero.setId(hero.getId());

        partialUpdatedHero.nome(UPDATED_NOME).eta(UPDATED_ETA).sex(UPDATED_SEX).segni(UPDATED_SEGNI).note(UPDATED_NOTE);

        restHeroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHero.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHero))
            )
            .andExpect(status().isOk());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
        Hero testHero = heroList.get(heroList.size() - 1);
        assertThat(testHero.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testHero.getEta()).isEqualTo(UPDATED_ETA);
        assertThat(testHero.getSex()).isEqualTo(UPDATED_SEX);
        assertThat(testHero.getSegni()).isEqualTo(UPDATED_SEGNI);
        assertThat(testHero.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    void patchNonExistingHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();
        hero.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHeroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hero.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hero))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();
        hero.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHeroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hero))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHero() throws Exception {
        int databaseSizeBeforeUpdate = heroRepository.findAll().size();
        hero.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHeroMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(hero)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Hero in the database
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHero() throws Exception {
        // Initialize the database
        heroRepository.saveAndFlush(hero);

        int databaseSizeBeforeDelete = heroRepository.findAll().size();

        // Delete the hero
        restHeroMockMvc
            .perform(delete(ENTITY_API_URL_ID, hero.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Hero> heroList = heroRepository.findAll();
        assertThat(heroList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
