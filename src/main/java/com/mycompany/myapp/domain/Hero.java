package com.mycompany.myapp.domain;

import com.mycompany.myapp.domain.enumeration.Sex;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Hero.
 */
@Entity
@Table(name = "hero")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Hero implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "eta", nullable = false)
    private Long eta;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @Column(name = "segni")
    private String segni;

    @Column(name = "note")
    private String note;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Hero id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public Hero nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getEta() {
        return this.eta;
    }

    public Hero eta(Long eta) {
        this.setEta(eta);
        return this;
    }

    public void setEta(Long eta) {
        this.eta = eta;
    }

    public Sex getSex() {
        return this.sex;
    }

    public Hero sex(Sex sex) {
        this.setSex(sex);
        return this;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getSegni() {
        return this.segni;
    }

    public Hero segni(String segni) {
        this.setSegni(segni);
        return this;
    }

    public void setSegni(String segni) {
        this.segni = segni;
    }

    public String getNote() {
        return this.note;
    }

    public Hero note(String note) {
        this.setNote(note);
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Hero user(User user) {
        this.setUser(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hero)) {
            return false;
        }
        return id != null && id.equals(((Hero) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Hero{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", eta=" + getEta() +
            ", sex='" + getSex() + "'" +
            ", segni='" + getSegni() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
