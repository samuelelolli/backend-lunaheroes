import { IUser } from 'app/entities/user/user.model';
import { Sex } from 'app/entities/enumerations/sex.model';

export interface IHero {
  id?: number;
  nome?: string;
  eta?: number;
  sex?: Sex;
  segni?: string | null;
  note?: string | null;
  user?: IUser | null;
}

export class Hero implements IHero {
  constructor(
    public id?: number,
    public nome?: string,
    public eta?: number,
    public sex?: Sex,
    public segni?: string | null,
    public note?: string | null,
    public user?: IUser | null
  ) {}
}

export function getHeroIdentifier(hero: IHero): number | undefined {
  return hero.id;
}
