import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { HeroService } from '../service/hero.service';

import { HeroComponent } from './hero.component';

describe('Hero Management Component', () => {
  let comp: HeroComponent;
  let fixture: ComponentFixture<HeroComponent>;
  let service: HeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HeroComponent],
    })
      .overrideTemplate(HeroComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(HeroComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(HeroService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.heroes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
