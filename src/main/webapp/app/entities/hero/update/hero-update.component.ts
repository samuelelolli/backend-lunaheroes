import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IHero, Hero } from '../hero.model';
import { HeroService } from '../service/hero.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { Sex } from 'app/entities/enumerations/sex.model';

@Component({
  selector: 'jhi-hero-update',
  templateUrl: './hero-update.component.html',
})
export class HeroUpdateComponent implements OnInit {
  isSaving = false;
  sexValues = Object.keys(Sex);

  usersSharedCollection: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    eta: [null, [Validators.required]],
    sex: [null, [Validators.required]],
    segni: [],
    note: [],
    user: [],
  });

  constructor(
    protected heroService: HeroService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hero }) => {
      this.updateForm(hero);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const hero = this.createFromForm();
    if (hero.id !== undefined) {
      this.subscribeToSaveResponse(this.heroService.update(hero));
    } else {
      this.subscribeToSaveResponse(this.heroService.create(hero));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHero>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(hero: IHero): void {
    this.editForm.patchValue({
      id: hero.id,
      nome: hero.nome,
      eta: hero.eta,
      sex: hero.sex,
      segni: hero.segni,
      note: hero.note,
      user: hero.user,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, hero.user);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): IHero {
    return {
      ...new Hero(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      eta: this.editForm.get(['eta'])!.value,
      sex: this.editForm.get(['sex'])!.value,
      segni: this.editForm.get(['segni'])!.value,
      note: this.editForm.get(['note'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }
}
